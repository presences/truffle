#!/bin/bash
# Bash Menu Script Example
echo "Presences command line tool"
echo "Ganache must be running for deploy"
PS3='Please enter your choice: '
options=("Create presence contract" "Deploy presence contract to ganache" "Create and Deploy" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Create presence contract")
				echo "Input lesson info: course name (ex: DSI), lesson date (ex: 15-01-2019_13:30), duration (in minutes, ex: 120) and room (ex: A5.1) [format: name;date;duration;room] [ex: DSI;15-01-2019_13:30;120;A5.1]:"
				read input
				arrIN=(${input//;/ })
				course=${arrIN[0]}
				date=${arrIN[1]}
				duration=${arrIN[2]}
				room=${arrIN[3]}
				echo "Adding information to smart contract..."
				sed -e "s/\${course}/$course/" -e "s/\${date}/$date/" -e "s/\${duration}/$duration/" -e "s/\${room}/$room/" ./Lesson-bk.sol | tee ./contracts/Lesson.sol
				echo "Added info to smart contract, located in ./contracts/Lesson.sol"
            ;;
        "Deploy presence contract to ganache")
				echo "Compiling contracts with truffle"
				truffle compile > /dev/null 2>&1 
			    truffle migrate --reset | grep "   > contract address:"	
				echo "Copy the second contract address (the address where Lesson smart contract is deployed), we need to place it in the react project"
				echo "Copy the generated file in  ./build/contracts/Lesson.json to components folder in ReactJS also"
            ;;
        "Create and Deploy")
				echo "Input lesson info: course name (ex: DSI), lesson date (ex: 15-01-2019_13:30), duration (in minutes, ex: 120) and room (ex: A5.1) [format: name;date;duration;room] [ex: DSI;15-01-2019_13:30;120;A5.1]:"
				read input
				arrIN=(${input//;/ })
				course=${arrIN[0]}
				date=${arrIN[1]}
				duration=${arrIN[2]}
				room=${arrIN[3]}
				echo "Adding information to smart contract..."
				sed -e "s/\${course}/$course/" -e "s/\${date}/$date/" -e "s/\${duration}/$duration/" -e "s/\${room}/$room/" ./Lesson-bk.sol | tee ./contracts/Lesson.sol
				echo "Added info to smart contract, located in ./contracts/Lesson.sol"
				echo "Compiling contracts with truffle"
				truffle compile > /dev/null 2>&1 
			    truffle migrate --reset | grep "   > contract address:"	
				echo "Copy the second contract address (the address where Lesson smart contract is deployed), we need to place it in the react project"
				echo "file on ../presences-frontend/src/utils/contractInfo.ts"
				echo "Copy the generated file in  ./build/contracts/Lesson.json to components folder in ReactJS also"
				echo "ex: cp build/contracts/Lesson.json ../presences-frontend/src/components/"
				;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
