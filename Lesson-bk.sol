pragma solidity >=0.4.21 <0.7.0;

contract Lesson {
	address public owner;
    mapping(address => bool) public whitelist;

	modifier onlyOwner {
        require(msg.sender == owner, "You are not the owner.");
        _;
    }

    /*modifier onlyWhitelist {
        require(whitelist[msg.sender], "You are not whitelisted.");
        _;
    }

    function setPermission(address user, bool isAllowed) public onlyOwner {
        whitelist[user] = isAllowed;
    }*/

	struct Student {
		uint number;
		string name;
		string location;
		uint evaluation;
		string note;
	}
	/*struct NumberInRoom {
		uint id;
		uint number;
	}*/
    string public course;
	string public date;
	string public room;
	uint public duration;
    mapping(uint => Student) public students;
	//mapping(uint => NumberInRoom) private numbers;
	uint[] public numbers;
    uint public studentsCount;
	bool private isOpen; 
    // Constructor
    constructor() public {
        course = "${course}";
		date = "${date}";
		duration = ${duration};
		room = "${room}";
		studentsCount = 0;
		isOpen = true;
    }


	function addStudent(string memory _name, string memory _location, uint _number) public {
		if(isOpen) {
				students[_number] = Student(_number, _name, _location, 0, "");
				numbers.push(_number);
				studentsCount++;
		}
	}

	function toggleIsOpen() public {
		isOpen = !isOpen;
	}
	function getIsOpen() public view returns (bool) {
		return isOpen;
	}
	/*function getStudentName(uint _number) public view returns (string memory) {
		return students[_number].name;
	}

	function getStudentLocation(uint _number) public view returns (string memory) {
		return students[_number].location;
	}*/

}
